import * as dotenv from 'dotenv';
import * as express from 'express';
import * as fs from 'fs';
import { google } from 'googleapis';

const port = 4444;

dotenv.config();

const config = {
  googleKey: process.env.TRANSLATION_GOOGLE_KEY,
  googleClientId: process.env.TRANSLATION_GOOGLE_CLIENT_ID,
  googleSecret: process.env.TRANSLATION_GOOGLE_SECRET,
  googleToken: process.env.TRANSLATION_GOOGLE_TOKEN_PATH,
  googleScopes: ['https://www.googleapis.com/auth/spreadsheets'],
  googleSpreadsheetId: process.env.TRANSLATION_SPREADSHEET_ID,
  googleSheetName: process.env.TRANSLATION_SHEET_NAME,
  mode: process.env.TRANSLATION_MODE,
  inputEnum: process.env.TRANSLATION_INPUT_ENUM_PATH,
  outputFolder: process.env.TRANSLATION_OUTPUT_FOLDER,
};

const action = process.argv[2];

const loggedIn = (auth) => {
  const sheets = google.sheets({ version: 'v4', auth });

  type Data = string[][];

  const upload = (data: Data) => {
    if (data.length === 0) {
      return;
    }

    const content = fs.readFileSync(config.inputEnum).toString();
    const lines = content.split('\n');

    const output = {};

    data.forEach((row) => {
      const key = row[0];

      [, output[key]] = row;
    });

    lines.forEach((line) => {
      if (!line.includes('=')) {
        return;
      }

      const [, key, rawComment] = line.split("'").map((part) => part.trim());
      const [, comment] = rawComment.split('//').map((part) => part.trim());

      output[key] = comment ?? '';
    });

    sheets.spreadsheets.values.update(
      {
        key: config.googleKey,
        spreadsheetId: config.googleSpreadsheetId,
        range: config.googleSheetName,
        requestBody: {
          values: Object.entries(output),
        },
        valueInputOption: 'RAW',
      },
      (err) => {
        if (err) {
          return console.log(`The API returned an error: ${err}`);
        }

        console.log('Uploaded');
      },
    );
  };

  const download = (data: Data) => {
    if (data.length === 0) {
      return;
    }

    const set = (object, keys, content) => {
      keys.slice(0, keys.length - 1).forEach((key) => {
        if (!(key in object)) {
          object[key] = {};
        }

        object = object[key];
      });

      object[keys[keys.length - 1]] = content;
    };

    const languages = data[0].slice(2);
    const output = {};

    languages.forEach((language) => {
      output[language] = {};
    });

    data.slice(1).forEach((row) => {
      const key = row[0].split('.');

      row.slice(2).forEach((text, index) => {
        set(output[languages[index]], key, text);
      });
    });

    languages.forEach((language) => {
      if (!fs.existsSync(`${config.outputFolder}/${language}`)) {
        fs.mkdirSync(`${config.outputFolder}/${language}`);
      }

      if (fs.existsSync(`${config.outputFolder}/${language}/text.json`)) {
        fs.unlinkSync(`${config.outputFolder}/${language}/text.json`);
      }

      fs.writeFileSync(
        `${config.outputFolder}/${language}/text.json`,
        JSON.stringify(output[language], null, 2),
      );
    });

    console.log('Downloaded');
  };

  const start = (data: Data) => {
    switch (action) {
      case 'upload':
        upload(data);
        break;
      case 'download':
        download(data);
        break;
    }
  };

  sheets.spreadsheets.values.get(
    {
      key: config.googleKey,
      spreadsheetId: config.googleSpreadsheetId,
      range: config.googleSheetName,
    },
    (err, res) => {
      if (err) {
        return console.log(`The API returned an error: ${err}`);
      }

      const data = res.data.values ?? [];

      start(data);
    },
  );
};

function getNewToken(oAuth2Client, callback) {
  const app = express();

  const server = app.listen(port, () => {});

  app.get('/', (req, res) => {
    const { code } = req.query;

    if (code) {
      res.status(200).send(code);

      server.close();

      oAuth2Client.getToken(code, (err, token) => {
        if (err) {
          return console.error(
            'Error while trying to retrieve access token',
            err,
          );
        }

        oAuth2Client.setCredentials(token);

        // Store the token to disk for later program executions
        fs.writeFile(config.googleToken, JSON.stringify(token), (err2) => {
          if (err2) {
            return console.error(err2);
          }

          console.log('Token stored to', config.googleToken);
        });

        callback(oAuth2Client);
      });
    } else {
      res.status(404).send(`Missing code!`);
    }
  });

  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: config.googleScopes,
  });

  console.log('Authorize this app by visiting this url:', authUrl);
}

const oAuth2Client = new google.auth.OAuth2(
  config.googleClientId,
  config.googleSecret,
  `http://localhost:${port}`,
);

fs.readFile(config.googleToken, (err, token) => {
  if (err) {
    return getNewToken(oAuth2Client, loggedIn);
  }

  oAuth2Client.setCredentials(JSON.parse(token.toString()));
  loggedIn(oAuth2Client);
});
